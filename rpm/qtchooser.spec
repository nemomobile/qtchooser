Name:	 qtchooser
Summary: Qt Chooser
Version: 39
Release: 0
License: LGPLv2 or GPLv3
URL:	 https://github.com/qtproject/qtsdk-qtchooser
Source0: http://macieira.org/qtchooser/qtchooser-%{version}.tar.gz
Requires: qt-default

%description
%{summary}

%prep
%setup -q -n qtchooser-%{version}/upstream

%build
make %{?_smp_mflags}

%install
make install INSTALL_ROOT=%{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/xdg/qtchooser
mkdir -p %{buildroot}%{_sysconfdir}/rpm/

# Add configuration file for qt5
echo "%{_libdir}/qt5/bin" > %{buildroot}%{_sysconfdir}/xdg/qtchooser/5.conf
echo "%{_libdir}" >> %{buildroot}%{_sysconfdir}/xdg/qtchooser/5.conf

%files
%defattr(-,root,root,-)
%doc LGPL_EXCEPTION.txt LICENSE.GPL LICENSE.LGPL
%dir %{_sysconfdir}/xdg/qtchooser
%{_sysconfdir}/xdg/qtchooser/5.conf
%{_bindir}/qtchooser
%{_bindir}/assistant
%{_bindir}/designer
%{_bindir}/lconvert
%{_bindir}/linguist
%{_bindir}/lrelease
%{_bindir}/lupdate
%{_bindir}/moc
%{_bindir}/pixeltool
%{_bindir}/qcollectiongenerator
%{_bindir}/qdbus
%{_bindir}/qdbuscpp2xml
%{_bindir}/qdbusviewer
%{_bindir}/qdbusxml2cpp
%{_bindir}/qdoc
%{_bindir}/qdoc3
%{_bindir}/qhelpconverter
%{_bindir}/qhelpgenerator
%{_bindir}/qmake
%{_bindir}/qlalr
%{_bindir}/qml
%{_bindir}/qmleasing
%{_bindir}/qmlimportscanner
%{_bindir}/qmllint
%{_bindir}/qml1plugindump
%{_bindir}/qmlbundle
%{_bindir}/qmlmin
%{_bindir}/qmlplugindump
%{_bindir}/qmlprofiler
%{_bindir}/qmlscene
%{_bindir}/qmltestrunner
%{_bindir}/qmlviewer
%{_bindir}/qtdiag
%{_bindir}/qtpaths
%{_bindir}/qtplugininfo
%{_bindir}/qtconfig
%{_bindir}/rcc
%{_bindir}/uic
%{_bindir}/uic3
%{_bindir}/xmlpatterns
%{_bindir}/xmlpatternsvalidator
%{_mandir}/man1/qtchooser.1.gz

